package de.higger.coronainfo.proxy;

import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@Provider
@Consumes({ MediaType.TEXT_PLAIN })
public class FTMessageBodyReader extends JacksonJsonProvider {

    @Override
    protected boolean hasMatchingMediaType(MediaType mediaType) {
        return true;
    }

}
package de.higger.coronainfo.proxy;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import de.higger.coronainfo.api.Landkreisdaten;

@Consumes({ "text/plain; charset=utf-8" })
public interface LandkreisdatenClient {

	@GET
	@Path("/query")
	Landkreisdaten getLandkreisdaten(@QueryParam("objectIds") String objectIds,
			@QueryParam("outFields") String outFields, @QueryParam("f") String format,
			@QueryParam("returnGeometry") boolean returnGeometry);
}

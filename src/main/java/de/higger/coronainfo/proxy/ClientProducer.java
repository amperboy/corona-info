
package de.higger.coronainfo.proxy;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClientProducer {

	@Bean
	public LandkreisdatenClient produceLandkreisdatenClient(
			@Value("${application.ws.arcgis.endpoint}") String endpoint) {
		return createResteasyWebProxy(LandkreisdatenClient.class, endpoint);
	}

	private <T> T createResteasyWebProxy(Class<T> proxyClass, String endpoint) {

		ClientBuilder clientBuilder = ResteasyClientBuilder.newBuilder();

		Client client = clientBuilder
//				.register(new LoggingClientRequestFilter(proxyClass))
//				.register(new LoggingClientResponseFilter(proxyClass))
				.register(ObjectMapperProvider.class)
				.register(FTMessageBodyReader.class)
				.build();
		ResteasyWebTarget target = (ResteasyWebTarget) client.target(endpoint);
		return target.proxy(proxyClass);
	}
}

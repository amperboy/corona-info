package de.higger.coronainfo.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import de.higger.coronainfo.api.ChartImageAttachment;

@Service
public class SendMailService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendMailService.class);

	@Autowired
	private JavaMailSender javaMailSender;

	@Value("${application.mail.sender}")
	private String sender;

	@Value("${application.mail.recipient}")
	private String recipient;

	public void sendReport(String mailcontent, List<ChartImageAttachment> attachments) {

		LocalDateTime now = LocalDateTime.now(ZoneId.of("Europe/Berlin"));
		String dateTime = now.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));

		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
			messageHelper.setTo(recipient.split(";"));
			messageHelper.setFrom(sender);
			messageHelper.setSubject("Corona 7 Tage Inzidenz - " + dateTime);
			messageHelper.setText(mailcontent, true);

			attachments.forEach(attachment -> {
				try {
					messageHelper.addInline(attachment.getCid(), new ByteArrayResource(attachment.getImage()
							.toByteArray()), ContentType.IMAGE_PNG.getMimeType());
				} catch (MessagingException e) {
					LOGGER.info("Failed to attach image for attribute {} cause {}", attachment.getCid(),
							e.getMessage());
				}
			});
		};

		javaMailSender.send(messagePreparator);
	}

}

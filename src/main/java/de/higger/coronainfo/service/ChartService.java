package de.higger.coronainfo.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.higger.coronainfo.api.ChartImageAttachment;
import de.higger.coronainfo.api.LandkreisdatenFeature;
import de.higger.coronainfo.api.LandkreisdatenFields;

@Service
public class ChartService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChartService.class);

	@Autowired
	private ProcessLandkreisdatenDataService processLandkreisdatenDataService;

	public ChartImageAttachment createLineChart(String attribute, List<LandkreisdatenFields> fields,
			Map<String, Map<LocalDate, Optional<LandkreisdatenFeature>>> historicData) {

		ChartImageAttachment attachment = new ChartImageAttachment();

		String label = fields.stream()
				.filter(field -> attribute.equals(field.getName()))
				.map(LandkreisdatenFields::getAlias)
				.findAny()
				.orElse(attribute);
		attachment.setAlternativeText(label);
		attachment.setCid("cid_" + attribute + ".png");

		JFreeChart lineChart = ChartFactory.createLineChart(label, "Tag", attribute,
				createDataset(attribute, historicData), PlotOrientation.VERTICAL, true, true, false);

		CategoryPlot plot = lineChart.getCategoryPlot();

		LineAndShapeRenderer renderer = new LineAndShapeRenderer();
		plot.setRenderer(renderer);

		int width = 640; /* Width of the image */
		int height = 480; /* Height of the image */

		try {
			ByteArrayOutputStream chart = new ByteArrayOutputStream();

			ChartUtils.writeChartAsPNG(chart, lineChart, width, height);

			attachment.setImage(chart);

			return attachment;

		} catch (IOException e) {

			LOGGER.warn("Failed to create chart for attribute {} with error {}", attribute, e.getMessage());
			return null;
		}

	}

	private CategoryDataset createDataset(String attribute,
			Map<String, Map<LocalDate, Optional<LandkreisdatenFeature>>> historicData) {

		DefaultCategoryDataset line_chart_dataset = new DefaultCategoryDataset();

		historicData.entrySet()
				.stream()
				.flatMap(entries -> entries.getValue()
						.entrySet()
						.stream())
				.map(Entry<LocalDate, Optional<LandkreisdatenFeature>>::getValue)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.sorted(Comparator.comparing(processLandkreisdatenDataService::extractLastUpdate))
				.forEach(landkreisdatenFeature -> {

					Optional<Double> yValue = processLandkreisdatenDataService
							.extractAttributeDoubleParsed(landkreisdatenFeature, attribute);
					yValue.ifPresent(value -> {

						LocalDate lastUpdate = processLandkreisdatenDataService
								.extractLastUpdate(landkreisdatenFeature);

						line_chart_dataset.addValue(value,
								processLandkreisdatenDataService.extractAttribute(landkreisdatenFeature, "GEN")
										.orElse(""),
								lastUpdate.format(DateTimeFormatter.ofPattern("dd.MM.")));
					});

				});

		return line_chart_dataset;
	}
}

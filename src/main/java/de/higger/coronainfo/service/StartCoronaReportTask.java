package de.higger.coronainfo.service;

import static java.util.stream.Collectors.groupingBy;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import de.higger.coronainfo.api.ChartImageAttachment;
import de.higger.coronainfo.api.Landkreisdaten;
import de.higger.coronainfo.api.LandkreisdatenFeature;
import de.higger.coronainfo.repository.LandkreisdatenRepository;

@Component
public class StartCoronaReportTask {

	private static final Logger LOGGER = LoggerFactory.getLogger(StartCoronaReportTask.class);

	private static final long ONE_MINUTE = 60 * 1000;

	@Value("${application.landkreise}")
	private String landkreise;

	@Value("${application.chartAttributes}")
	private String chartAttributes;

	@Value("${application.pollDelayMinutes}")
	private long pollDelayMinutes;

	@Value("${application.sendImmediately}")
	private boolean sendImmediately;

	@Autowired
	private FetchDataService fetchDataService;

	@Autowired
	private CreateReportService createReportService;

	@Autowired
	private LandkreisdatenRepository landkreisdatenRepository;

	@Autowired
	private ProcessLandkreisdatenDataService processLandkreisdatenDataService;

	@Autowired
	private ChartService chartService;

	@Autowired
	private SendMailService mailService;

	@Scheduled(cron = "0 0 4 * * *", zone = "")
	public void startDailyReport() {
		LOGGER.info("Send planned report");
		task().run();
	}

	@PostConstruct
	public void sendReport() {
		if (sendImmediately) {
			LOGGER.info("Send report after application start immediately");
			task().run();
		}
	}

	private TimerTask task() {

		return new TimerTask() {

			@Override
			public void run() {

				Landkreisdaten landkreisdaten = fetchDataService.receive(landkreise);
				Optional<LocalDate> lastUpdate = processLandkreisdatenDataService.extractLastUpdate(landkreisdaten);

				if (lastUpdate.isPresent() && lastUpdate.get().isEqual(LocalDate.now())) {

					LOGGER.info("Send report {}", lastUpdate);

					landkreisdaten.setReceivedAt(LocalDateTime.now());
					landkreisdaten.getFeatures()
							.stream()
							.forEach(landkreisdatenFeature -> landkreisdatenFeature.setLandkreisdaten(landkreisdaten));

					landkreisdatenRepository.save(landkreisdaten);

					LocalDate minReceviedAt = LocalDate.now()
							.minus(Period.ofDays(7));

					List<Landkreisdaten> lastDaysLandkreisdaten = landkreisdatenRepository
							.findByReceivedAtIsAfter(minReceviedAt.atStartOfDay());

					Map<String, Map<LocalDate, Optional<LandkreisdatenFeature>>> historicData = groupByObjectIdAndLastUpdateDay(
							lastDaysLandkreisdaten);

					List<ChartImageAttachment> attachments = Arrays.stream(chartAttributes.split(","))
							.map(attribute -> chartService.createLineChart(attribute, landkreisdaten.getFields(),
									historicData))
							.collect(Collectors.toList());

					String html = createReportService.asHtml(landkreisdaten, attachments);

					mailService.sendReport(html, attachments);

				} else {

					LOGGER.info("Last update is to old {}. Reschedule Task. Check again in {} minutes", lastUpdate,
							pollDelayMinutes);
					new Timer().schedule(StartCoronaReportTask.this.task(), pollDelayMinutes * ONE_MINUTE);
				}

			}

			private Map<String, Map<LocalDate, Optional<LandkreisdatenFeature>>> groupByObjectIdAndLastUpdateDay(
					List<Landkreisdaten> lastDaysLandkreisdaten) {

				return lastDaysLandkreisdaten.stream()
						.map(Landkreisdaten::getFeatures)
						.flatMap(List<LandkreisdatenFeature>::stream)
						.collect(groupingBy(processLandkreisdatenDataService::extractObjectId,
								groupingBy(processLandkreisdatenDataService::extractLastUpdate,
										Collectors.maxBy(Comparator.comparing(feature -> feature.getLandkreisdaten()
												.getReceivedAt())))));

			}
		};
	}

}

package de.higger.coronainfo.service;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

import de.higger.coronainfo.api.ChartImageAttachment;
import de.higger.coronainfo.api.Landkreisdaten;
import de.higger.coronainfo.api.LandkreisdatenFeature;
import de.higger.coronainfo.api.LandkreisdatenFields;

@Service
public class CreateReportService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CreateReportService.class);

	@Value("${application.ws.arcgis.endpoint}")
	private String arcgisEndpoint;

	@Value("${application.dataAttributes}")
	private String dataAttributes;

	public String asHtml(Landkreisdaten landkreisdaten, List<ChartImageAttachment> lineCharts) {

		MustacheFactory mf = new DefaultMustacheFactory();
		Mustache mustache = mf.compile("mustache/corona-info.mustache");

		List<String> dataReportFields = Arrays.asList(dataAttributes.split(","));

		List<LandkreisdatenFields> fields = landkreisdaten.getFields()
				.stream()
				.filter(field -> dataReportFields.contains(field.getName()))
				.collect(Collectors.toList());
		
		List<Set<Entry<String, String>>> features = landkreisdaten.getFeatures()
				.stream()
				.map(LandkreisdatenFeature::getAttributes)
				.map(Map::entrySet)
				.map(
						entrySet -> entrySet.stream().filter(entry -> dataReportFields.contains(entry.getKey())).collect(Collectors.toCollection(LinkedHashSet::new))
				)
				.collect(Collectors.toList());

		Map<String, Object> context = new HashMap<>();
		context.put("quelle", arcgisEndpoint);
		context.put("fields", fields);
		context.put("features", features);

		context.put("lineCharts", lineCharts);

		StringWriter writer = new StringWriter();
		try {
			mustache.execute(writer, context)
					.flush();
		} catch (IOException e) {
			LOGGER.warn("Failed to create corona report", e);
		}
		return writer.toString();
	}

}

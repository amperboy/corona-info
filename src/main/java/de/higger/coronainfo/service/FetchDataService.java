package de.higger.coronainfo.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import de.higger.coronainfo.api.Landkreisdaten;
import de.higger.coronainfo.api.LandkreisdatenFields;
import de.higger.coronainfo.proxy.LandkreisdatenClient;

@Service
public class FetchDataService {

	private static final boolean RETURN_GEOMETRY = false;

	private static final String OUTPUT_FORMAT = "pjson";

	@Value("${application.dataAttributes}")
	private String dataAttributes;

	@Value("${application.chartAttributes}")
	private String chartAttributes;

	@Value("${application.calc100kAttributes}")
	private String calc100kAttributes;

	@Autowired
	private ProcessLandkreisdatenDataService processLandkreisdatenDataService;

	@Autowired
	private LandkreisdatenClient landkreisdatenClient;

	public Landkreisdaten receive(String... objectIds) {

		String objectIdsJoined = String.join(",", objectIds);

		List<String> calculatedFields = asStream(calc100kAttributes).map(attribute -> attribute + "_per_100k")
				.collect(Collectors.toList());

		String requestAttributes = Stream
				.concat(asStream(dataAttributes),
						Stream.concat(asStream(chartAttributes), asStream(calc100kAttributes)))
				.distinct()
				.filter(attribute -> !calculatedFields.contains(attribute))
				.collect(Collectors.joining(","));

		Landkreisdaten landkreisdaten = landkreisdatenClient.getLandkreisdaten(objectIdsJoined, requestAttributes,
				OUTPUT_FORMAT, RETURN_GEOMETRY);

		extendWithCaluculated100kFields(landkreisdaten);

		return landkreisdaten;
	}

	private void extendWithCaluculated100kFields(Landkreisdaten landkreisdaten) {
		List<LandkreisdatenFields> additionalFields = asStream(calc100kAttributes)
				.map(attribute -> getFieldByName(landkreisdaten, attribute))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(field -> {
					LandkreisdatenFields newField = new LandkreisdatenFields();
					newField.setAlias(field.getAlias() + "/100.000 EW");
					newField.setName(field.getName() + "_per_100k");
					return newField;
				})
				.collect(Collectors.toList());

		landkreisdaten.getFields()
				.addAll(additionalFields);

		landkreisdaten.getFeatures()
				.stream()
				.forEach(feature -> {

					processLandkreisdatenDataService.extractAttributeIntegerParsed(feature, "EWZ")
							.ifPresent(einwohnerzahl -> {
								double faktor100k = einwohnerzahl / 100000.0;

								asStream(calc100kAttributes)
										.map(atrribute -> processLandkreisdatenDataService
												.extractAttributeDoubleParsed(feature, atrribute)
												.map(value -> Pair.of(atrribute, value)))
										.filter(Optional::isPresent)
										.map(Optional::get)
										.forEach(pair -> {
											String newAttributeKey = pair.getFirst() + "_per_100k";
											double newValue = pair.getSecond() / faktor100k;

											feature.getAttributes()
													.put(newAttributeKey, Double.toString(newValue));
										});
								;

							});

				});
	}

	private Optional<LandkreisdatenFields> getFieldByName(Landkreisdaten landkreisdaten, String attribute) {
		return landkreisdaten.getFields()
				.stream()
				.filter(field -> attribute.equals(field.getName()))
				.findAny();
	}

	private Stream<String> asStream(String joinedAttributes) {
		return Arrays.stream(joinedAttributes.split(","));
	}
}

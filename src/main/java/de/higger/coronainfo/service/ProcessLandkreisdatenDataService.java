package de.higger.coronainfo.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import de.higger.coronainfo.api.Landkreisdaten;
import de.higger.coronainfo.api.LandkreisdatenFeature;

@Service
public class ProcessLandkreisdatenDataService {

	private static final String ATTRIBUTE_KEY_LAST_UPDATE = "last_update";
	private static final String ATTRIBUTE_KEY_OBJECT_ID = "OBJECTID";

	private static final DateTimeFormatter ATTRIBUTE_LAST_UPDATE_FORMAT = DateTimeFormatter
			.ofPattern("dd.MM.yyyy, HH:mm 'Uhr'");

	public Optional<String> extractAttribute(LandkreisdatenFeature landkreisdatenFeature, String attribute) {
		return extractionStream(landkreisdatenFeature, attribute, String.class).findAny();
	}

	public Optional<Double> extractAttributeDoubleParsed(LandkreisdatenFeature landkreisdatenFeature,
			String attribute) {
		return extractionStream(landkreisdatenFeature, attribute, String.class).map(Double::parseDouble)
				.findAny();
	}

	public Optional<Long> extractAttributeIntegerParsed(LandkreisdatenFeature landkreisdatenFeature, String attribute) {
		return extractionStream(landkreisdatenFeature, attribute, String.class).map(Long::parseLong)
				.findAny();
	}

	public String extractObjectId(LandkreisdatenFeature landkreisdatenFeature) {
		return this.extractionStream(landkreisdatenFeature, ATTRIBUTE_KEY_OBJECT_ID, String.class)
				.findAny()
				.orElse("");
	}

	public LocalDate extractLastUpdate(LandkreisdatenFeature landkreisdatenFeature) {

		return extractionStream(landkreisdatenFeature, ATTRIBUTE_KEY_LAST_UPDATE, String.class)
				.map(lastUpdate -> LocalDate.parse(lastUpdate, ATTRIBUTE_LAST_UPDATE_FORMAT))
				.collect(Collectors.minBy(Comparator.naturalOrder()))
				.orElse(LocalDate.now());

	}

	public <T> Optional<T> extractAttribute(LandkreisdatenFeature landkreisdatenFeature, String attribute,
			Class<T> targetClass) {
		return extractionStream(landkreisdatenFeature, attribute, targetClass).findAny();
	}

	private <T> Stream<T> extractionStream(LandkreisdatenFeature landkreisdatenFeature, String attribute,
			Class<T> targetClass) {
		return landkreisdatenFeature.getAttributes()
				.entrySet()
				.stream()
				.filter(entry -> attribute.equals(entry.getKey()))
				.map(Entry<String, String>::getValue)
				.filter(targetClass::isInstance)
				.map(targetClass::cast);
	}

	public Optional<LocalDate> extractLastUpdate(Landkreisdaten landkreisdaten) {

		return landkreisdaten.getFeatures()
				.stream()
				.flatMap(feature -> extractionStream(feature, ATTRIBUTE_KEY_LAST_UPDATE, String.class))
				.filter(lastUpdateDate -> lastUpdateDate != null)
				.map(String::trim)
				.filter(lastUpdateDate -> !"".equals(lastUpdateDate))
				.map(lastUpdate -> LocalDate.parse(lastUpdate, ATTRIBUTE_LAST_UPDATE_FORMAT))
				.collect(Collectors.minBy(Comparator.naturalOrder()));

	}

}

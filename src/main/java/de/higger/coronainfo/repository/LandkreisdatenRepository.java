package de.higger.coronainfo.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.higger.coronainfo.api.Landkreisdaten;

@Repository
public interface LandkreisdatenRepository extends JpaRepository<Landkreisdaten, Long> {

	List<Landkreisdaten> findByReceivedAtIsAfter(LocalDateTime minReceivedAt);

}

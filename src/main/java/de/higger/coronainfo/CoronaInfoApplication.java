package de.higger.coronainfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CoronaInfoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoronaInfoApplication.class, args);
	}

}

package de.higger.coronainfo.api;

import java.util.Map;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class LandkreisdatenFeature {

	@Id	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_landkreisdaten_feature")
	@SequenceGenerator(name = "seq_landkreisdaten_feature", allocationSize = 5)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	private Landkreisdaten landkreisdaten;

	@ElementCollection(fetch = FetchType.EAGER)
	@JoinTable(name = "LANDKREISDATEN_FEATURE_KEY_VALUE", joinColumns = @JoinColumn(name = "LANDKREISDATEN_FEATURE_ID"))
	@MapKeyColumn(name = "feature_value")
	private Map<String, String> attributes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Landkreisdaten getLandkreisdaten() {
		return landkreisdaten;
	}

	public void setLandkreisdaten(Landkreisdaten landkreisdaten) {
		this.landkreisdaten = landkreisdaten;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	@Override
	public String toString() {
		return "LandkreisdatenFeature [attributes=" + attributes + "]";
	}

}

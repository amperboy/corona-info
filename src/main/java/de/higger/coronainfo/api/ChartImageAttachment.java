package de.higger.coronainfo.api;

import java.io.ByteArrayOutputStream;

public class ChartImageAttachment {

	private String cid;

	private String alternativeText;

	private ByteArrayOutputStream image;

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getAlternativeText() {
		return alternativeText;
	}

	public void setAlternativeText(String alternativeText) {
		this.alternativeText = alternativeText;
	}

	public ByteArrayOutputStream getImage() {
		return image;
	}

	public void setImage(ByteArrayOutputStream image) {
		this.image = image;
	}

}

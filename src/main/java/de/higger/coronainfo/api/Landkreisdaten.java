package de.higger.coronainfo.api;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class Landkreisdaten {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_landkreisdaten")
	@SequenceGenerator(name = "seq_landkreisdaten", allocationSize = 5)
	private Long id;

	private LocalDateTime receivedAt;

	@Transient
	private List<LandkreisdatenFields> fields;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "landkreisdaten", fetch = FetchType.EAGER)
	private List<LandkreisdatenFeature> features;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getReceivedAt() {
		return receivedAt;
	}

	public void setReceivedAt(LocalDateTime receivedAt) {
		this.receivedAt = receivedAt;
	}

	public List<LandkreisdatenFields> getFields() {
		return fields;
	}

	public void setFields(List<LandkreisdatenFields> fields) {
		this.fields = fields;
	}

	public List<LandkreisdatenFeature> getFeatures() {
		return features;
	}

	public void setFeatures(List<LandkreisdatenFeature> features) {
		this.features = features;
	}

	@Override
	public String toString() {
		return "Landkreisdaten [fields=" + fields + ", features=" + features + "]";
	}

}

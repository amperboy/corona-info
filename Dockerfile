FROM openjdk:14-alpine
ENV spring.mail.properties.mail.smtp.starttls.enable=true
ENV spring.mail.properties.mail.smtp.auth=true
ENV spring.mail.password=PASSWORD
ENV spring.mail.username=USERNAME@gmail.com
ENV spring.mail.port=587
ENV spring.mail.host=smtp.gmail.com
ENV application.mail.sender=webmaster@localhost
ENV application.mail.recipient=webmaster@localhost
ENV application.landkreise=284
ENV application.chartAttributes=cases7_per_100k
ENV application.ws.arcgis.attributes=OBJECTID,GEN,last_update,cases7_per_100k
ENV application.pollDelayMinutes=15
ENV application.sendImmediately=false

RUN apk upgrade --update && apk add --no-cache tzdata freetype fontconfig ttf-dejavu
RUN  cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime

WORKDIR /application
COPY target/unzip/BOOT-INF/lib /application/lib
COPY target/unzip/META-INF /application/META-INF
COPY target/unzip/BOOT-INF/classes /application

CMD ["sh","-c","java -cp .:lib/* de.higger.coronainfo.CoronaInfoApplication"]
